<?php
namespace alar\utils;
use Psr\SimpleCache\CacheInterface;
use Kodus\Cache\FileCache;

class wrap {
    static protected $resolution='Ymd';
    static protected $expire=3600*24;
    /** @var CacheInterface $cache */
    static protected $cache;
    /** @var FileCache $filecache */
    static private $filecache;
    static public function setCache(CacheInterface $cache) {
        self::$cache=$cache;
    }
    static public function getCache() : CacheInterface {
        if (!self::$cache) {
            if (!self::$filecache) {
                self::$filecache=new FileCache(sys_get_temp_dir(). DIRECTORY_SEPARATOR . 'wrap', self::$expire);
            }
            self::$cache=self::$filecache;
        }
        return self::$cache;
    }
    static public function clearCache() {
        self::getCache()->clear();
    }
    static public function setResolution($resolution) {
        switch(strtoupper($resolution)) {
            case 'Y':
                self::$resolution='Y';
                self::$expire=3600*24*365;
                break;
            case 'M':
                self::$resolution='Ym';
                self::$expire=3600*24*31;
                break;
            case 'D':
                self::$resolution='Ymd';
                self::$expire=3600*24;
                break;
            case 'H':
                self::$resolution='YmdH';
                self::$expire=3600;
                break;
            case 'I':
                self::$resolution='YmdHi';
                self::$expire=60;
                break;
        }
    }
    static public function deleteUrl($url,$context=null) {
        self::getCache()->delete(self::buildKey($url, $context));
    }
    static public function getUrl($url,$debug=false) {
        return self::getUrlWithContext($url,null,$debug);
    }
    static public function buildKey($url,$context) {
        $key=$url;
        if (is_resource($context)) {
            $c=array_change_key_case(stream_context_get_options($context)['http'],CASE_LOWER);
            $method=$c['method'];
            if (empty($method)) $method='GET';
            $p=$c['content'];
            ksort($p);
            if ($c) {
                $key.=serialize($p);
            }
        }
        elseif (is_array($context)) {
            ksort($context);
            $key.=serialize($context);
            
        }
        $host=parse_url($url,PHP_URL_HOST);
        if ($debug) echo "Using key '$key'\n";
        $name="wrap_{$seed}_{$host}_".md5($key);
        return $name;
    }
    static public function getUrlWithContext($url,$context=null, $debug=false) {
        $seed=date(self::$resolution);
        $key=self::buildKey($url,$context);
        $data=self::getCache()->get($key,null);
        if (!is_null($data)) return $data;
        if ($debug) fprintf(STDERR,"Running [%s] due to failed cache %s built on %s\n",$url,$key,$seed);
        $result=file_get_contents($url,null,$context);
        if (!is_null($result)) {
            self::getCache()->set($key,$result,self::$expire);
        }
        return $result;
    }
    static public function wrap($method,...$args) {
        $seed=date(self::$resolution);
        if (is_array($method)) {
            $url='http://'.get_class($method[0]).'/'.$method[1];
        }
        else {
            $url='http://'.$method;
        }
        $key=self::buildKey($url, $args);
        $result=self::getCache()->get($key,null);
        if (!is_null($result)) return $result;
        if ($debug) fprintf(STDERR,"Running [%s] due to failed cache %s built on %s\n",$key,$seed);
        $result=call_user_func_array($method,$args);
        if (!is_null($result)) {
            self::getCache()->set($key,$resultmself::$expire);
        }
        return $result;
        
    }
    static public function __callstatic($method,$args) {
        return self::wrap($method,$args);
    }
    static public function file_get_html($url,$debug=false) {
        return str_get_html(self::getUrl($url,$debug));
    }
}
