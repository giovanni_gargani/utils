# README #

alar/template template engine

### What is this repository for? ###

* alar/template is a template engine compatible with PERL::HtmlTemplate 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

    composer install alar/utils

* Configuration
* Dependencies
* Database configuration
* How to run tests

    ./vendor/bin/phpunit -c Tests/
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact