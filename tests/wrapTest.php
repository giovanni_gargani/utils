<?php
namespace alar\utils;
use Psr\SimpleCache\CacheInterface;
use alar\utils\wrap;
use PHPUnit\Framework\TestCase;
use Kodus\Cache\FileCache;

/**
 * wrap test case.
 */
class nullcache implements CacheInterface{
    function deleteMultiple($keys) {
        
    }
    function setMultiple($values,$ttl=NULL) {
        
    }
    function getMultiple($keys,$default=NULL) {
        
    }
    function set($key, $value, $ttl = NULL) {
        
    }
    function get($key,$default=NULL) {
        return $default;        
    }
    function clear() {
        
    }
    function delete($key) {
        
    }
    function has($key) {
        return false;
    }
    
}
class wrapTest extends TestCase
{
    function testCacheInjection() {
        self::assertTrue(wrap::getCache() instanceOf FileCache);
        wrap::setCache(new nullcache());
        self::assertTrue(wrap::getCache() instanceOf nullcache);
        
    }
}
