<?php
use alar\utils\getopts;
use PHPUnit\Framework\TestCase;
/**
 *
 * getopts. Nothing else needed.
 * @author giovanni
 *
 */

final class getoptsTest extends TestCase{
	/**
	 * Builds an options object
	 * 
	 * $opt=new getopts([  
	 * 
	 * 	  '&remote'=>[false,'Switches to test.titleapi.link'],
	 *   
	 * 	  '&client:'=>['demo',$validId],
	 *   
	 * 	  ]);
	 *   
	 * $opt->showHelp($message,true);
	 *   
	 * $opt->constants();
	 *   
	 * 
	 * @param array $config
	 */


	/**
	 * construct
	 */
	public function testConstruct()
	{
	    
	    $opt=new getopts([
	     	  '&remote'=>[false,'Switches to test.titleapi.link'],
	           '&client:'=>[21,'Customer id'],
	    ]);
	    self::assertEquals(21, $opt->opts('client'),'Using opts(<varname>)');
	    self::assertEquals(21, $opt->client(),'using shortcut -><varname>()');
	}
}