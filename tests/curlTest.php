<?php


/**
 * curl test case.
 */
use PHPUnit\Framework\TestCase;
use alar\utils\curl;

class curlTest extends TestCase
{
    function testGetField() {
        $curl=new curl();
        $link="https://www.pippo.com?id=12";
        $linkNoQuery="https://www.pippo.com";
        self::assertEquals('12', $curl->getField($link,'id'),"qs and field present, field requested");
        self::assertNull( $curl->getField($link,'ud'),"qs and field not present");
        self::assertArrayHasKey( 'id',$curl->getField($link),"qs and field present present,field not requested");
        self::assertNull($curl->getField($linkNoQuery,'id'),"qs and field present, field requested");
        self::assertNull( $curl->getField($linkNoQuery,'ud'),"qs and field not present");
    }

}

